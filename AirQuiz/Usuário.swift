//
//  Usuário.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 07/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation

let ÚnicoUsuário = Usuário()

class Usuário {
    enum Empresa: Int {
        case Cessna = 0
        case Embraer
        case Bombardier
        case Boeing
        case Airbus
        case AllAircrafts
    }
    
    enum Nível: Int {
        case Noob = 0
        case Aprentice
        case Basic
        case Intermediate
        case Expert
        case Master
    }
    
    /* Constante de ajuda para bundle. */
    static let bundle = NSBundle.mainBundle()
    
    /* Pontuação do usuário (score). */
    private lazy var pontuaçãoPrivada: Int = 0
    var pontuação: Int {
        get {
            return pontuaçãoPrivada
        }
    }
    
    /* Array de níveis do usuário.
    Níveis para cada level. */
    private lazy var níveisPrivados: [Int] = []
    var níveis: [Int] {
        get {
            return níveisPrivados
        }
    }
    
    /* Um array padrão para os dados de usuário. */
    let arrayPadrãoDoUsuário: [String: AnyObject] = ["Pontuação": 0,
        "Níveis": [Int](count: Empresa.AllAircrafts.rawValue + 1,
            repeatedValue: Nível.Noob.rawValue)]
    
    /// MARCA - Métodos públicos.
    
    /// MARCA - Métodos para salvamento e carregamento.
    
    func salvarOsDados() {
        let padrõesPrincipal = NSUserDefaults.standardUserDefaults()
        
        // Coloca os dados do aplicativo nos defaults.
        padrõesPrincipal.setObject(pontuaçãoPrivada, forKey: "Pontuação")
        padrõesPrincipal.setObject(níveisPrivados, forKey: "Níveis")
        
        // Sincroniza defaults.
        padrõesPrincipal.synchronize();
    }
    
    func carregarOsDados() {
        let padrõesPrincipal = NSUserDefaults.standardUserDefaults()
        
        // Faz a primeira leitura do defaults e verifica se o valor de pontuação existe.
        if let leituraDaPontuação = padrõesPrincipal.objectForKey("Pontuação") as? Int {
            pontuaçãoPrivada = leituraDaPontuação
            níveisPrivados = padrõesPrincipal.objectForKey("Níveis") as! [Int]
        } else {
            println("Primeira execução") // DEBUG
            
            // Carrega os dados padrão.
            reiniciarOsDados()
        }
        
        println("\(pontuaçãoPrivada) - \(níveisPrivados)") // DEBUG
    }
    
    func reiniciarOsDados() {
        let padrõesPrincipal = NSUserDefaults.standardUserDefaults()
        
        // Registra e sincroniza defaults.
        padrõesPrincipal.registerDefaults(arrayPadrãoDoUsuário)
        padrõesPrincipal.synchronize()
        
        pontuaçãoPrivada = padrõesPrincipal.objectForKey("Pontuação") as! Int
        níveisPrivados = padrõesPrincipal.objectForKey("Níveis") as! [Int]
    }
    
    /// MARCA - Métodos para gerenciar os dados do usuário.
    
    func incrementaPontuação() {
        ++pontuaçãoPrivada
    }
    
    func incrementaNível(paraAEmpresa empresa: Empresa, doNível: Nível) {
        // Verifica se empresa existe.
        if empresa.rawValue >= Empresa.Cessna.rawValue &&
            empresa.rawValue <= Empresa.AllAircrafts.rawValue {
                // Pega nível.
                let nível = níveisPrivados[empresa.rawValue]
                
                // Incrementa se já não for o máximo.
                if doNível == Nível(rawValue: nível)
                && nível >= Nível.Noob.rawValue && nível < Nível.Master.rawValue {
                        níveisPrivados[empresa.rawValue] = nível + 1
                }
        } else {
            println("Essa empresa não existe.") // DEBUG
        }
    }
    
    func pegarNível(paraAEmpresa empresa: Empresa) -> Int? {
        // Verifica se empresa existe.
        if empresa.rawValue >= Empresa.Cessna.rawValue &&
            empresa.rawValue <= Empresa.AllAircrafts.rawValue {
                return níveisPrivados[empresa.rawValue]
        } else {
            println("Essa empresa não existe.") // DEBUG
            return nil
        }
    }
    
    /// MARCA - Métodos para descrições dos enumeradores.
    
    class func descriçãoParaEmpresa(empresa: Empresa) -> String? {
        switch empresa {
        case .Cessna:
            return "CESSNA"
        case .Embraer:
            return "EMBRAER"
        case .Bombardier:
            return "BOMBARDIER"
        case .Boeing:
            return "BOEING"
        case .Airbus:
            return "AIRBUS"
        case .AllAircrafts:
            return "TODOS"
        default:
            return nil
        }
    }
    
    class func descriçãoParaNível(nível: Nível) -> String? {
        switch nível {
        case .Noob:
            return "Noob"
        case .Aprentice:
            return "Aprendiz"
        case .Basic:
            return "Básico"
        case .Intermediate:
            return "Intermediário"
        case .Expert:
            return "Experiente"
        case .Master:
            return "Mestre"
        default:
            return nil
        }
    }
    
}
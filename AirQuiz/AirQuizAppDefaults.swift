//
//  AirQuizAppDefaults.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 08/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation
import UIKit

class AirQuizAppDefaults {
    /* Mostra alerta na cara do usuário :D */
    class func apresentaAlerta(comTítulo título: String,
        eMensagem mensagem: String, naViewController controller: UIViewController,
        comAçãoNoSim closure: (UIAlertAction!) -> Void) {
            // Cria o alerta.
            let alerta = UIAlertController(
                title: título,
                message: mensagem,
                preferredStyle: UIAlertControllerStyle.Alert
            )
            
            // Adiciona ação do não.
            let noAction = UIAlertAction(
                title: "NÃO",
                style: UIAlertActionStyle.Cancel,
                handler: nil
            )
            alerta.addAction(noAction)
            
            // Adiciona ação do sim.
            let yesAction = UIAlertAction(
                title: "SIM",
                style: UIAlertActionStyle.Destructive,
                handler: closure
            )
            alerta.addAction(yesAction)
            
            // Apresenta o alerta.
            controller.presentViewController(alerta, animated: true, completion: nil)
    }
}
//
//  GameViewController.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 07/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import UIKit

class GameViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var viewCabeçalho: UIView!
    @IBOutlet weak var labelPontuação: UILabel!
    @IBOutlet weak var labelWins: UILabel!
    
    @IBOutlet weak var labelPergunta: UILabel!
    @IBOutlet weak var imgAvião: UIImageView!
    
    @IBOutlet weak var picker: UIPickerView!
    
    var empresa: BaseDeAviões.Empresa?
    var nível: Usuário.Nível?
    var questão: (QuestãoDoJogo, String, Avião)?
    
    // Lista de escolhas.
    private var escolhas: [String]?
    private var contagemDeVitórias: Int = 0
    private var indexCorretoDeEscolha: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if empresa == nil
            || nível == nil {
                self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // Coloca texto inicial para vitórias.
        labelWins.text = String("\(contagemDeVitórias)/6")
        
        
        // Gera primeira pergunta.
        gerarPergunta()
        
        labelPergunta.text = questão!.1
        if questão!.0.contémImagem {
            imgAvião.image = UIImage(named: questão!.2.imagem)
        } else {
            imgAvião.image = UIImage(named: "AirQuiz.png")
        }
        labelPontuação.text = String(ÚnicoUsuário.pontuação)
        
        /* Decorações. */
        
        // Coloca uma borda na view de pontiação.
        var bordaDeBaixo = CALayer()
        bordaDeBaixo.backgroundColor = UIColor(red: 0.0, green: 0.0,
            blue: 0.0, alpha: 0.3).CGColor
        bordaDeBaixo.frame = CGRectMake(0, viewCabeçalho.frame.height - 0.5,
            CGRectGetWidth(viewCabeçalho.frame), 0.5)
        viewCabeçalho.layer.addSublayer(bordaDeBaixo)
        
        // Deixa cor de texto do picker branco.
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        labelPontuação.text = String(ÚnicoUsuário.pontuação)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// MARCA - Gerador de perguntas.
    
    func gerarPergunta() {
        // Pega questão diferente da anterior.
        var próximaQuestão = ÚnicoJogo.pegarQuestãoAleatória(paraEmpresa: empresa!,
            eNível: nível!)
        if questão != nil {
            while próximaQuestão.2 == questão!.2 {
                próximaQuestão = ÚnicoJogo.pegarQuestãoAleatória(paraEmpresa: empresa!,
                    eNível: nível!)
            }
        }
        
        questão = próximaQuestão
        
        var conjuntoDeEscolhas = Set<String>()
        
        switch (próximaQuestão.0.tipo) {
        case .Modelo:
            // Pega 4 aviões falsos.
            
            // Insere avião real.
            conjuntoDeEscolhas.insert(próximaQuestão.2.modelo)
            
            while conjuntoDeEscolhas.count != 5 {
                var a = ÚnicoJogo.pegarAviãoAleatório(daEmpresa: empresa!)
                if !conjuntoDeEscolhas.contains(a!.modelo) {
                    conjuntoDeEscolhas.insert(a!.modelo)
                }
            }
            
            // Converte conjunto para array.
            escolhas = [String](conjuntoDeEscolhas)
            
            // From http://stackoverflow.com/questions/56648/whats-the-best-way-to-shuffle-an-nsmutablearray
            for index in 0..<escolhas!.count {
                var remainingCount = escolhas!.count - index
                var exchangeIndex = index + Int(arc4random_uniform(UInt32(remainingCount)))
                let objectAtI = escolhas![index]
                escolhas![index] = escolhas![exchangeIndex]
                escolhas![exchangeIndex] = objectAtI
            }
            
        case .Motor:
            // Três motores.
            escolhas = ["Mono", "Bi", "Hélice"]
            
            indexCorretoDeEscolha = próximaQuestão.2.motor.rawValue
            
        case .Tipo:
            // Alguns tipos.
            escolhas = ["Jato Executivo", "Turbo Executivo", "Multi Uso",
                "Agricola", "Passageiro", "Militar", "Pessoal e Treinamento"]
            
            indexCorretoDeEscolha = próximaQuestão.2.tipo.rawValue
            
        case .AnoDeLançamento:
            escolhas = [String(questão!.2.decada), String(questão!.2.decada - 10),
                String(questão!.2.decada - 20), String(questão!.2.decada + 10),
                String(questão!.2.decada + 20)]
            
            // From http://stackoverflow.com/questions/56648/whats-the-best-way-to-shuffle-an-nsmutablearray
            for index in 0..<escolhas!.count {
                var remainingCount = escolhas!.count - index
                var exchangeIndex = index + Int(arc4random_uniform(UInt32(remainingCount)))
                let objectAtI = escolhas![index]
                escolhas![index] = escolhas![exchangeIndex]
                escolhas![exchangeIndex] = objectAtI
            }
            
        case .Velocidade:
            escolhas = [String(questão!.2.velocidade), String(questão!.2.velocidade - 100),
                String(questão!.2.velocidade - 200), String(questão!.2.velocidade + 100),
                String(questão!.2.velocidade + 200)]
            
            // From http://stackoverflow.com/questions/56648/whats-the-best-way-to-shuffle-an-nsmutablearray
            for index in 0..<escolhas!.count {
                var remainingCount = escolhas!.count - index
                var exchangeIndex = index + Int(arc4random_uniform(UInt32(remainingCount)))
                let objectAtI = escolhas![index]
                escolhas![index] = escolhas![exchangeIndex]
                escolhas![exchangeIndex] = objectAtI
            }
        }
    }
    
    /// MARCA - Pickerview delegate e datasource.
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return escolhas!.count
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: escolhas![row],
            attributes: [NSForegroundColorAttributeName : UIColor(white: 1.0, alpha: 1.0)])
    }
    
    @IBAction func responderQuestão(sender: AnyObject) {
        // Incrementa os pontos.
        let questãoEscolhida = picker.selectedRowInComponent(0)
        
        var venceu = false
        
        switch (questão!.0.tipo) {
        case .Modelo:
            if questão!.2.modelo == escolhas![questãoEscolhida] {
                venceu = true
            }
        case .Motor:
            if questãoEscolhida == indexCorretoDeEscolha {
                venceu = true
            }
        case .Tipo:
            if questãoEscolhida == indexCorretoDeEscolha {
                venceu = true
            }
        case .AnoDeLançamento:
            if questão!.2.decada == escolhas![questãoEscolhida].toInt() {
                venceu = true
            }
        case .Velocidade:
            if questão!.2.velocidade == escolhas![questãoEscolhida].toInt() {
                venceu = true
            }
        }

        if venceu {
            ÚnicoUsuário.incrementaPontuação()
            
            if contagemDeVitórias < 6 {
                contagemDeVitórias++
                
                if contagemDeVitórias == 6 {
                    ÚnicoUsuário.incrementaNível(
                        paraAEmpresa: Usuário.Empresa(rawValue: empresa!.rawValue)!,
                        doNível: self.nível!
                    )
                    
                    /// GRAVA OS DADOS!
                    ÚnicoUsuário.salvarOsDados()
                    
                    AirQuizAppDefaults.apresentaAlerta(comTítulo: "Parabéns",
                        eMensagem: "Você venceu esse nível. Deseja sair desse jogo?",
                        naViewController: self, comAçãoNoSim: {
                            (alerta: UIAlertAction?) -> Void in
                            self.navigationController?.popViewControllerAnimated(true)
                    })
                }
            }
            // Coloca texto para vitória.
            labelWins.text = String("😀 \(contagemDeVitórias)/6")
            
        } else {
            if contagemDeVitórias > 0
            && contagemDeVitórias < 6 {
                contagemDeVitórias--
            }
            
            // Coloca texto para derrota.
            labelWins.text = String("😕 \(contagemDeVitórias)/6")
        }
        
        // Gera a nova pergunta.
        gerarPergunta()
        
        labelPergunta.text = questão!.1
        if questão!.0.contémImagem {
            imgAvião.image = UIImage(named: questão!.2.imagem)
        } else {
            imgAvião.image = UIImage(named: "AirQuiz.png")
        }
        labelPontuação.text = String(ÚnicoUsuário.pontuação)
        
        picker.reloadComponent(0)
        
        // Volta para estado inicial do picker.
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
}

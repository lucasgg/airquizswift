//
//  BaseDeAviões.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 07/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation

let ÚnicaBaseDeAviões = BaseDeAviões()

class BaseDeAviões {
    enum Empresa: Int {
        case Cessna = 0
        case Embraer
    }
    
    /* O array de empresas contém conjuntos de aviões.
    Foi escrito na seguinte notação para facilitar a interpretação. */
    private lazy var empresas = [Set<Avião>]()
    
    /* Quantidade de componentes do banco de aviões.
    É a quantidade de componentes em cada linha. */
    private let númeroDeComponentesDoBancoDeAviões = 7
    
    init() {
        self.popularEmpresas()
    }
    
    /// MARCA - Métodos públicos.
    
    /* Retorna a quantidade de empresas carregadas. */
    func quantidadeDeEmpresas() -> Int {
        return empresas.count
    }
    
    /* Retorna a quantidade de aviões de uma determinada empresa. */
    func quantidadeDeAviões(paraAEmpresa empresa: Empresa) -> Int {
        return empresas[empresa.rawValue].count
    }
    
    /* Pega conjunto de aviões de uma determinada empresa */
    func pegaConjuntoDeAviões(daEmpresa empresa: Empresa) -> Set<Avião>? {
        if (empresa.rawValue >= Empresa.Cessna.rawValue
            && empresa.rawValue <= Empresa.Embraer.rawValue) {
            return empresas[empresa.rawValue]
        } else {
            return nil
        }
    }
    
    /// MARCA - Métodos privados.
    
    /* Popula as empresas na inicialização lendo do banco de dados. */
    private func popularEmpresas() {
        // Lista de strings para os nomes dos arquivos.
        for arquivo in ["cessna", "embraer"] {
            // Verifica se existe o caminho do arquivo.
            if let caminhoDoArquivo = Usuário.bundle.pathForResource(arquivo, ofType: "txt") {
                // Pega as linhas do arquivo.
                let linhasDoArquivo: [String]? = { () -> [String]? in
                    // Pega conteúdo do arquivo.
                    if let conteudoDoArquivo = String(contentsOfFile: caminhoDoArquivo,
                        encoding: NSUTF8StringEncoding, error: nil) {
                            // Se conteúdo existir retorna os componentes separados por pulo de linha.
                            return conteudoDoArquivo.componentsSeparatedByCharactersInSet(
                                NSCharacterSet.newlineCharacterSet()
                            )
                    } else {
                        // Se conteúdo não existir retorna nil.
                        println("Conteúdo do arquivo \(arquivo).txt não pode ser lido.") // DEBUG
                        return nil
                    }
                    }() // CLOSURE CALL
                
                // Verifica se linhas existem.
                if linhasDoArquivo != nil {
                    // Declara o conjunto de aviões que vou adicionar no array de empresas.
                    var conjuntoDeAviões: Set<Avião>?
                    
                    // Se existir arquivo, popula um conjunto de aviões.
                    for linhaDoAvião in linhasDoArquivo! {
                        // Se linha for vazia, continua para próxima linha.
                        if linhaDoAvião.isEmpty {
                            continue
                        }
                        
                        // Pega os componentes da linha.
                        let componentesDaLinha = linhaDoAvião.componentsSeparatedByString("|")
                        
                        // Constrói avião.
                        if let avião = constróiAviãoAPartirDeComponentes(componentesDaLinha) {
                            // Cria conjunto se já não estiver sido criado.
                            if conjuntoDeAviões == nil {
                                conjuntoDeAviões = Set<Avião>()
                            }
                            
                            // Adiciona avião ao conjunto.
                            conjuntoDeAviões!.insert(avião)
                        } else {
                            // Se dados do avião são inválidos.
                            println("Dados na linha \(linhaDoAvião) não são válidos.") // DEBUG
                            continue
                        }
                    }
                    
                    if conjuntoDeAviões != nil {
                        // Adiciona conjunto de aviões no array de empresas.
                        empresas.append(conjuntoDeAviões!)
                    }
                } else {
                    // Se não existirem linhas, vai para próximo arquivo.
                    continue
                }
            } else {
                // Se arquivo não existe.
                println("Arquivo \(arquivo).txt não encontrado.") // DEBUG
                continue
            }
        }
    }
    
    /* Verifica se avião é válido */
    private func constróiAviãoAPartirDeComponentes(componentes: [String]) -> Avião? {
        // Se número de componentes for diferente, então falta alguma coisa.
        if (componentes.count != númeroDeComponentesDoBancoDeAviões) {
            println("Número de componentes não bate: \(componentes.count).") // DEBUG
            return nil
        }
        
        var modelo: String?
        var tipo: Avião.TipoDeAvião?
        var motor: Avião.TipoDeMotor?
        var decada: Int?
        var largura: String?
        var velocidade: Int?
        var imagem: String?
        
        for (índice, componente) in enumerate(componentes) {
            // Não pode ter componentes nulas.
            if componente.isEmpty {
                println("Nenhuma das componentes podem estar vazias.") // DEBUG
                return nil
            }
            
            // Itera sobre cada índice que corresponde a um componente do avião.
            switch índice {
            case 0:
                modelo = componente
            case 1:
                if componente == "JATO_EXECUTIVO" {
                    tipo = .JatoExecutivo
                } else if componente == "TURBO_EXECUTIVO" {
                    tipo = .TurboExecutivo
                } else if componente == "MULTI_USO" {
                    tipo = .MultiUso
                } else if componente == "AGRICOLA" {
                    tipo = .Agricola
                } else if componente == "PASSAGEIRO" {
                    tipo = .Passageiro
                } else if componente == "MILITAR" {
                    tipo = .Militar
                } else if componente == "PESSOAL_TREINAMENTO" {
                    tipo = .PessoalETreinamento
                } else {
                    println("Tipo de avião desconhecido: \(componente)") // DEBUG
                    return nil
                }
            case 2:
                if componente == "MONOMOTOR" {
                    motor = .Mono
                } else if componente == "BIMOTOR" {
                    motor = .Bi
                } else if componente == "TURBO_HELICE" {
                    motor = .Helice
                } else {
                    println("Tipo de motor desconhecido: \(componente)") // DEBUG
                    return nil
                }
            case 3:
                if let inteiroDaDecada = componente.toInt() {
                    decada = inteiroDaDecada
                } else {
                    println("Valor inválido para a década: \(componente)") // DEBUG
                    return nil
                }
            case 4:
                largura = componente
            case 5:
                if let inteiroDaVelocidade = componente.toInt() {
                    velocidade = inteiroDaVelocidade
                } else {
                    println("Valor inválido para a velocidade: \(componente)") // DEBUG
                    return nil
                }
            case 6:
                if let caminhoDaImagem = Usuário.bundle.pathForResource(componente, ofType: "") {
                    imagem = componente
                } else {
                    println("Imagem não existe no bundle: \(componente)") // DEBUG
                    return nil
                }
            default:
                println("Não deveria ter chego aqui...") // DEBUG
                return nil
            }
        }
        
        return Avião(modelo: modelo!, tipo: tipo!,
            motor: motor!, decada: decada!, largura: largura!,
            velocidade: velocidade!, imagem: imagem!)
    }
    
}
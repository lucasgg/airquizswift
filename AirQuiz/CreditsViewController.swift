//
//  CreditsViewController.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 07/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import UIKit

class CreditsViewController: UIViewController {
    
    @IBOutlet weak var caixaDeCréditos: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Insere o conteúdo de créditos.txt na caixa.
        if let caminhoDoArquivo = Usuário.bundle.pathForResource("créditos", ofType: "txt") {
            caixaDeCréditos.insertText(
                String(contentsOfFile: caminhoDoArquivo, encoding: NSUTF8StringEncoding, error: nil)!
            )
        } else {
            println("Arquivo de crédito não encontrado.") // DEBUG
        }
        
        // Arredonda o textview de créditos.
        caixaDeCréditos.layer.cornerRadius = 4.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// MARCA - Ações com o usuário.
    
    @IBAction func recomeçarJogo(sender: UIBarButtonItem) {
        AirQuizAppDefaults.apresentaAlerta(comTítulo: "Atenção",
            eMensagem: "Tem certeza que deseja reiniciar os dados de jogo?",
            naViewController: self) { (ação: UIAlertAction!) -> Void in
                ÚnicoUsuário.reiniciarOsDados()
        }
    }
    
}

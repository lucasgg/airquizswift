//
//  MenuViewController.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 07/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var viewCabeçalho: UIView!
    @IBOutlet weak var labelPontuação: UILabel!
    
    @IBOutlet weak var tableDesafios: UITableView!
    
    private var empresaSelecionada: Int = 0
    private var nívelSelecionado: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Decorações. */
        
        // Coloca uma borda na view de pontiação.
        var bordaDeBaixo = CALayer()
        bordaDeBaixo.backgroundColor = UIColor(red: 0.0, green: 0.0,
            blue: 0.0, alpha: 0.3).CGColor
        bordaDeBaixo.frame = CGRectMake(0, viewCabeçalho.frame.height - 0.5,
            CGRectGetWidth(viewCabeçalho.frame), 0.5)
        viewCabeçalho.layer.addSublayer(bordaDeBaixo)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        labelPontuação.text = String(ÚnicoUsuário.pontuação)
        
        tableDesafios.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// MARCA - Navegação.
    
    @IBAction func toCreditsView(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("toCreditsView", sender: self)
    }
    
    // Preparação para a navegação entre as views.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController is GameViewController {
            let gvc = segue.destinationViewController as! GameViewController
            
            gvc.empresa = BaseDeAviões.Empresa(rawValue: empresaSelecionada)
            gvc.nível = Usuário.Nível(rawValue: nívelSelecionado)
        }
    }
    
    /// MARCA - Delegate e Datasource da Tableview.
    
    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            return Usuário.Empresa.AllAircrafts.rawValue + 1
    }
    
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            // Pega a célula necessária.
            var célula = tableView.dequeueReusableCellWithIdentifier("menuCell",
                forIndexPath: indexPath) as? MenuViewCell
            
            // Configuração da célula.
            if let célulaDoMenu = célula {
                // Coloca o texto da empresa na célula.
                célulaDoMenu.labelEmpresa.text = Usuário.descriçãoParaEmpresa(
                    Usuário.Empresa(rawValue: indexPath.row)!
                )
                
                let nívelDoUsuárioParaEssaEmpresa = Usuário.Nível(rawValue:
                    ÚnicoUsuário.pegarNível(paraAEmpresa:
                        Usuário.Empresa(rawValue: indexPath.row)!
                        )!
                    )!
                
                // Coloca o texto do nível.
                célulaDoMenu.labelNível.text = Usuário.descriçãoParaNível(
                    nívelDoUsuárioParaEssaEmpresa
                )
                
                // Pega a parte da empresa no nome do ícone.
                var nomeDoÍconeDeAvião = { () -> String in
                    switch (Usuário.Empresa(rawValue: indexPath.row)!) {
                    case .Cessna:
                        return "cessna";
                    case .Bombardier:
                        return "bombardier";
                    case .Airbus:
                        return "airbus";
                    case .Boeing:
                        return "boeing";
                    case .Embraer:
                        return "embraer";
                    case .AllAircrafts:
                        return "allplanes";
                    default:
                        return ""
                    }
                    }()
                
                for var i = 0; i < Usuário.Nível.Master.rawValue; ++i {
                    // Coloca as imagens nos níveis.
                    let componente = célulaDoMenu.imgColeçãoDeNíveis[i]
                    
                    // Pega segundo nome da imagens (que significa o número de estrelas)
                    let númeroDeEstrelas = { () -> String in
                        switch (Usuário.Nível(rawValue: i)!) {
                        case .Noob:
                            return "one"
                        case .Aprentice:
                            return "two"
                        case .Basic:
                            return "tree"
                        case .Intermediate:
                            return "four"
                        case .Expert:
                            return "five"
                        default:
                            return ""
                        }
                        }()
                    
                    // Verifica se usuário já ganhou um nível.
                    // Se sim, pega a imagem diamante, no contrário pega a branca.
                    if nívelDoUsuárioParaEssaEmpresa.rawValue > i {
                        componente.hidden = false
                        
                        let imagem = String(format: "%@_%@_star_diamond.png",
                            nomeDoÍconeDeAvião, númeroDeEstrelas);
                        
                        componente.image = UIImage(named: imagem);
                    } else if nívelDoUsuárioParaEssaEmpresa.rawValue == i {
                        componente.hidden = false
                        
                        let imagem = String(format: "%@_%@_star_white.png",
                            nomeDoÍconeDeAvião, númeroDeEstrelas);
                        
                        componente.image = UIImage(named: imagem);
                    } else {
                        componente.hidden = true
                    }
                }
                
                return célulaDoMenu
            } else {
                println("Célula menuCell não encontrada.") // DEBUG
                return UITableViewCell()
            }
    }
    
    func tableView(tableView: UITableView,
        editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
            return nil
    }
    
    /// MARCA - Gestos.
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        let célulaSelecionada = touch.locationInView(tableDesafios)
        
        if let empresaIndexPath = tableDesafios.indexPathForRowAtPoint(célulaSelecionada) {
            empresaSelecionada = empresaIndexPath.row
            
            if empresaSelecionada <= Usuário.Empresa.Embraer.rawValue {
                var imagemSelecionada: UIImageView?
                
                for item in (tableDesafios.cellForRowAtIndexPath(empresaIndexPath)
                    as! MenuViewCell).imgColeçãoDeNíveis {
                        // Pega o ponto do clique.
                        let ponto = tableDesafios.convertPoint(célulaSelecionada, toView: item as UIView)
                        
                        if ponto.x >= 0 && ponto.y >= 0 {
                            imagemSelecionada = item
                        }
                }
                
                if let imagem = imagemSelecionada {
                    let nível = ÚnicoUsuário.pegarNível(paraAEmpresa:
                        Usuário.Empresa(rawValue: empresaSelecionada)!
                        )!
                    
                    for (index, object) in enumerate((tableDesafios.cellForRowAtIndexPath(empresaIndexPath)
                        as! MenuViewCell).imgColeçãoDeNíveis) {
                            if nível + 1 <= index {
                                break;
                            }
                            
                            if object == imagem {
                                nívelSelecionado = index
                                self.performSegueWithIdentifier("toGameView", sender: self)
                            }
                    }
                }
            }
        }
        
        return true
    }
}

//
//  Avião.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 06/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation

class Avião: Hashable {
    enum TipoDeAvião: Int {
        case JatoExecutivo = 0
        case TurboExecutivo
        case MultiUso
        case Agricola
        case Passageiro
        case Militar
        case PessoalETreinamento
    }
    
    enum TipoDeMotor: Int {
        case Mono = 0
        case Bi
        case Helice
    }
    
    let modelo: String
    let tipo: TipoDeAvião
    let motor: TipoDeMotor
    let decada: Int
    let largura: String
    let velocidade: Int
    let imagem: String
    
    init(modelo: String, tipo: TipoDeAvião, motor: TipoDeMotor,
        decada: Int, largura: String, velocidade: Int, imagem: String) {
            self.modelo = modelo
            self.tipo = tipo
            self.motor = motor
            self.decada = decada
            self.largura = largura
            self.velocidade = velocidade
            self.imagem = imagem
    }
    
    /// Marca - Protocólo Hashable.
    
    var hashValue: Int {
        return modelo.hashValue
    }
    
}

/// Equatable.
func ==(lhs: Avião, rhs: Avião) -> Bool {
    return lhs.modelo == rhs.modelo
}

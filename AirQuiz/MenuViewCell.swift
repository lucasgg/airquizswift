//
//  MenuViewCell.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 08/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {
    
    @IBOutlet weak var labelEmpresa: UILabel!
    @IBOutlet weak var labelNível: UILabel!
    @IBOutlet var imgColeçãoDeNíveis: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

//
//  QuestãoDoJogo.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 08/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation

class QuestãoDoJogo: Hashable {
    enum TipoDeQuestão: Int {
        case Modelo = 0
        case Motor
        case AnoDeLançamento
        case Tipo
        case Velocidade
    }
    
    let questão: String
    let contémImagem: Bool
    let tipo: TipoDeQuestão
    
    init(questão: String, contémImagem: Bool, tipo: TipoDeQuestão) {
        self.questão = questão
        self.contémImagem = contémImagem
        self.tipo = tipo
    }
    
    /// Marca - Protocólo Hashable.
    
    var hashValue: Int {
        return tipo.hashValue
    }
}

/// Equatable.
func ==(lhs: QuestãoDoJogo, rhs: QuestãoDoJogo) -> Bool {
    return lhs.tipo == rhs.tipo
}
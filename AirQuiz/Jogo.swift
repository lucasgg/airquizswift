//
//  Jogo.swift
//  AirQuiz
//
//  Created by Lucas Guimarães Gonçalves on 08/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation

let ÚnicoJogo = Jogo()

class Jogo {
    
    /* O conjunto de questões do jogo. */
    private lazy var conjuntoDeQuestões = Set<QuestãoDoJogo>()
    
    /* Quantidade de componentes do banco de questões.
    É a quantidade de componentes em cada linha. */
    private let númeroDeComponentesDoBancoDeQuestões = 3
    
    init() {
        self.popularQuestões()
    }
    
    /// MARCA - Métodos públicos.
    
    func pegarQuestãoFormatada(doAvião avião: Avião, comQuestão questão: QuestãoDoJogo) -> String {
        if questão.tipo != QuestãoDoJogo.TipoDeQuestão.Modelo {
            return String(format: questão.questão, avião.modelo)
        } else {
            return questão.questão
        }
    }
    
    func pegarAviãoAleatório(daEmpresa empresa: BaseDeAviões.Empresa) -> Avião? {
        if let conjuntoDeAviões = ÚnicaBaseDeAviões.pegaConjuntoDeAviões(daEmpresa: empresa) {
            // Pego de http://stackoverflow.com/questions/28489867/how-to-get-random-element-from-a-set-in-swift
            // Não sei se existe um método melhor para pegar um índice aleatório.
            let randomOffset = Int(arc4random_uniform(UInt32(conjuntoDeAviões.count)))
            return conjuntoDeAviões[advance(conjuntoDeAviões.startIndex, randomOffset)]
        } else {
            println("Essa empresa não existe.") // DEBUG
            return nil
        }
    }
    
    func pegarQuestãoAleatória(paraEmpresa empresa: BaseDeAviões.Empresa,
        eNível nível: Usuário.Nível) -> (QuestãoDoJogo, String, Avião) {
            let randomLimit: QuestãoDoJogo.TipoDeQuestão
            
            switch nível {
            case .Noob:
                randomLimit = .Modelo
            case .Aprentice:
                randomLimit = .Motor
            case .Basic:
                randomLimit = .AnoDeLançamento
            case .Intermediate:
                randomLimit = .Tipo
            case .Expert:
                randomLimit = .Velocidade
            case .Master:
                randomLimit = .Velocidade
            }
            
            // Pega um tipo aleatório.
            let randomOffset = Int(arc4random_uniform(UInt32(randomLimit.rawValue + 1)))
            
            // Pega questão selecionada.
            var questão: QuestãoDoJogo? = nil
            for q in conjuntoDeQuestões {
                if q.tipo.rawValue == randomOffset {
                    questão = q
                    break
                }
            }
            
            let avião = pegarAviãoAleatório(daEmpresa: empresa)
            
            // Retorna tupla com questão e avião.
            return (
                questão!,
                pegarQuestãoFormatada(doAvião: avião!, comQuestão: questão!),
                avião!
            )
    }
    
    /// MARCA - Métodos privados.
    
    /* Popula as questões na inicialização lendo do banco de dados. */
    private func popularQuestões() {
        // Verifica se existe o caminho do arquivo.
        if let caminhoDoArquivo = Usuário.bundle.pathForResource("questões",
            ofType: "txt") {
                // Pega as linhas do arquivo.
                let linhasDoArquivo: [String]? = { () -> [String]? in
                    // Pega conteúdo do arquivo.
                    if let conteudoDoArquivo = String(contentsOfFile: caminhoDoArquivo,
                        encoding: NSUTF8StringEncoding, error: nil) {
                            // Se conteúdo existir retorna os componentes separados por pulo de linha.
                            return conteudoDoArquivo.componentsSeparatedByCharactersInSet(
                                NSCharacterSet.newlineCharacterSet()
                            )
                    } else {
                        // Se conteúdo não existir retorna nil.
                        println("Conteúdo do arquivo questões.txt não pode ser lido.") // DEBUG
                        return nil
                    }
                    }() // CLOSURE CALL
                
                // Verifica se linhas existem.
                if linhasDoArquivo != nil {
                    // Se existir arquivo, popula um conjunto de questões.
                    for linhaDaQuestão in linhasDoArquivo! {
                        // Se linha for vazia, continua para próxima linha.
                        if linhaDaQuestão.isEmpty {
                            continue
                        }
                        
                        // Pega os componentes da linha.
                        let componentesDaLinha = linhaDaQuestão.componentsSeparatedByString("|")
                        
                        // Constrói questão.
                        if let questão = constróiQuetãoAPartirDeComponentes(componentesDaLinha) {
                            // Adiciona questão ao conjunto.
                            conjuntoDeQuestões.insert(questão)
                        } else {
                            // Se dados da questão são inválidos.
                            println("Dados na linha \(linhaDaQuestão) não são válidos.") // DEBUG
                            continue
                        }
                    }
                }
        } else {
            // Se arquivo não existe.
            println("Arquivo questões.txt não encontrado.") // DEBUG
        }
    }
    
    /* Verifica se questão é válida */
    private func constróiQuetãoAPartirDeComponentes(componentes: [String]) -> QuestãoDoJogo? {
        // Se número de componentes for diferente, então falta alguma coisa.
        if (componentes.count != númeroDeComponentesDoBancoDeQuestões) {
            println("Número de componentes não bate: \(componentes.count).") // DEBUG
            return nil
        }
        
        var questão: String?
        var contémImagem: Bool?
        var tipo: QuestãoDoJogo.TipoDeQuestão?
        
        for (índice, componente) in enumerate(componentes) {
            // Não pode ter componentes nulas.
            if componente.isEmpty {
                println("Nenhuma das componentes podem estar vazias.") // DEBUG
                return nil
            }
            
            // Itera sobre cada índice que corresponde a um componente da questão.
            switch índice {
            case 0:
                if componente == "AIRCRAFT" {
                    contémImagem = true
                } else if componente == "ONLYTEXT" {
                    contémImagem = false
                } else {
                    println("Atributo de conter imagem não é válido: \(componente)") // DEBUG
                    return nil
                }
            case 1:
                questão = componente
            case 2:
                if componente == "MODEL" {
                    tipo = .Modelo
                } else if componente == "MOTOR" {
                    tipo = .Motor
                } else if componente == "YEAR" {
                    tipo = .AnoDeLançamento
                } else if componente == "TYPE" {
                    tipo = .Tipo
                } else if componente == "VELOCITY" {
                    tipo = .Velocidade
                } else {
                    println("Tipo de questão desconhecida: \(componente)") // DEBUG
                    return nil
                }
            default:
                println("Não deveria ter chego aqui...") // DEBUG
                return nil
            }
        }
        
        return QuestãoDoJogo(questão: questão!,
            contémImagem: contémImagem!, tipo: tipo!)
    }
    
}